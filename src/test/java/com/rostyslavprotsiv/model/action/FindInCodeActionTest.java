package com.rostyslavprotsiv.model.action;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FindInCodeActionTest {
    @ParameterizedTest
    @MethodSource("getData")
    void testFindComments(String path, List<String> comments) {
        FindInCodeAction action = new FindInCodeAction();
        try {
            assertEquals(comments, action.findComments(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Stream<Arguments> getData() {
        String path1 = "Mainn.java";
        List<String> comments1 = Arrays.asList("Abracadabra", "Kuda dadadada " +
                        "dada",
                "This is the best method", "Menu showww methooood", "barabra");
        String path2 = "./src/main/java/com/rostyslavprotsiv/view/Menu.java";
        List<String> comments2 = Arrays.asList("This is my menu class", "Some" +
                " comments...");
        String path3 = "./src/main/java/com/rostyslavprotsiv/model/creator" +
                "/BottleCreator.java";
        List<String> comments3 = Arrays.asList("imitation of reading from DB",
                "process of reading from DB");
        return Stream.of(
                Arguments.of(path1, comments1),
                Arguments.of(path2, comments2),
                Arguments.of(path3, comments3)
        );
    }
}
