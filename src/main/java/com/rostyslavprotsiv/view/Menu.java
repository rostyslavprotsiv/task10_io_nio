package com.rostyslavprotsiv.view;

import java.io.IOException;
import java.util.List;

/**
 * This is my menu class
 */
public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(0, "Serialize");
        menu.put(1, "Deserialize");
        menu.put(2, "Run method without buffer");
        menu.put(3, "Out all buffered methods speed");
        menu.put(4, "Show example with my InputStream");
        menu.put(5, "Out all comments from the inputted file");
        menu.put(6, "Print all subfiles");
        menu.put(7, "Exit");
        methodsForMenu.put(0, this::printSerialized);
        methodsForMenu.put(1, this::printDeserialized);
        methodsForMenu.put(2, this::printRunWithoutBuffer);
        methodsForMenu.put(3, this::printGotBufferComparing);
        methodsForMenu.put(4, this::printGotExample);
        methodsForMenu.put(5, this::printAllComments);
        methodsForMenu.put(6, this::printAllSubfiles);
        methodsForMenu.put(7, this::quit);
    }

    public void show() {
        out();
    }
//Some comments...

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for task10_IO_NIO");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void printSerialized() {
        CONTROLLER.serialize();
        LOGGER.info("Serialized!");
    }

    private void printDeserialized() {
        LOGGER.info(CONTROLLER.deserialize());
    }

    private void printRunWithoutBuffer() {
        LOGGER.info("Running : ");
        CONTROLLER.runWithoutBuffer();
        LOGGER.info("The end : ");
    }

    private void printGotBufferComparing() {
        LOGGER.info("\n" + CONTROLLER.getBufferComparing());
    }

    private void printGotExample() {
        LOGGER.info(CONTROLLER.getExampleWithMyInputStream());
    }

    private void printAllComments() {
        LOGGER.info("Please, input path : ");
        List<String> allComments;
        try {
            allComments = CONTROLLER.getFoundComments(scan.next());
        } catch (IOException e) {
            badInput();
            String logMessage = "Used incorrect path";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
            return;
        }
        allComments.forEach(LOGGER::info);
    }

    private void printAllSubfiles() {
        LOGGER.info("Please, input path : ");
        LOGGER.info("\n" + CONTROLLER.getAllSubfiles(scan.next()));
    }
}
