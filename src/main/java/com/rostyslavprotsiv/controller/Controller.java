package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.*;
import com.rostyslavprotsiv.model.creator.BottleCreator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final BottleCreator BOTTLE_CREATOR = new BottleCreator();
    private final SerializeAction SERIALIZE_ACTION = new SerializeAction();
    private final CompareBufferAction COMPARE_BUFFER_ACTION = new CompareBufferAction();
    private final MyInputStreamAction MY_INPUT_STREAM_ACTION = new MyInputStreamAction();
    private final FindInCodeAction FIND_IN_CODE_ACTION = new FindInCodeAction();
    private final FileAction FILE_ACTION = new FileAction();

    public void serialize() {
        SERIALIZE_ACTION.serialize(BOTTLE_CREATOR.readFromDB());
    }

    public String deserialize() {
        return SERIALIZE_ACTION.deserialize()
                .toString();
    }

    public void runWithoutBuffer() {
        COMPARE_BUFFER_ACTION.readWithoutBuffer();
    }

    public String getBufferComparing() {
        return COMPARE_BUFFER_ACTION.getAllComparedMethods();
    }

    public String getExampleWithMyInputStream() {
        return MY_INPUT_STREAM_ACTION.showExample();
    }

    public List<String> getFoundComments(String path) throws IOException {
        return FIND_IN_CODE_ACTION.findComments(path);
    }

    public String getAllSubfiles(String path) {
        String result = FILE_ACTION.getAllSubfiels(path);
        if(result == null) {
            String logMessage = "Bad inputted path";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
        }
        return result;
    }
}
