package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Bottle;

import java.io.*;

public class SerializeAction {
    private String path = ResourceManager.INSTANCE.getString("path1");

    public void serialize(Bottle bottle) {
        try (ObjectOutputStream out =
                     new ObjectOutputStream(new FileOutputStream(path))) {
            out.writeObject(bottle);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Bottle deserialize() {
        try (ObjectInputStream out =
                     new ObjectInputStream(new FileInputStream(path))) {
            return (Bottle) out.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
