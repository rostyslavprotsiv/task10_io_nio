package com.rostyslavprotsiv.model.action.clientsforrun;

import com.rostyslavprotsiv.model.action.client.NetClient;

public class RunClient {
    public static void main(String[] args) {
        new NetClient().run();
    }
}
