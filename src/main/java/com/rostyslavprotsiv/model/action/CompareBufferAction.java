package com.rostyslavprotsiv.model.action;

import java.io.*;

public class CompareBufferAction {
    private String path = ResourceManager.INSTANCE.getString("path2");

    public void readWithoutBuffer() {
        try (InputStream input = new FileInputStream(path)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long readWithBuffer100MB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024 * 1024 * 100)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer50MB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024 * 1024 * 50)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer25MB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024 * 1024 * 25)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer8MB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024 * 1024 * 8)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer1MB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024 * 1024 * 1)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer500KB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024 * 500)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer100KB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024 * 100)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer10KB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024 * 10)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer1KB() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 1024)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer500B() {
        long start = System.nanoTime();
        try (InputStream input =
                     new BufferedInputStream(new FileInputStream(path),
                             1 * 500)) {
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getAverageTime100MB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer100MB();
        }
        compared.append("Read with Buffer 100 MB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime50MB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer50MB();
        }
        compared.append("Read with Buffer 50 MB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime25MB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer25MB();
        }
        compared.append("Read with Buffer 25 MB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime8MB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer8MB();
        }
        compared.append("Read with Buffer 8 MB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime1MB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer1MB();
        }
        compared.append("Read with Buffer 1 MB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime500KB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer500KB();
        }
        compared.append("Read with Buffer 500 KB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime100KB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer100KB();
        }
        compared.append("Read with Buffer 100 KB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime10KB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer10KB();
        }
        compared.append("Read with Buffer 10 KB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime1KB() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer1KB();
        }
        compared.append("Read with Buffer 1 KB : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAverageTime500B() {
        StringBuilder compared = new StringBuilder();
        long time = 0;
        for (int i = 0; i < 10; i++) {
            time += readWithBuffer500B();
        }
        compared.append("Read with Buffer 500 B : \t")
                .append(time / 10)
                .append("\n");
        return compared.toString();
    }

    public String getAllComparedMethods() {
        StringBuilder compared = new StringBuilder();
        getAverageTime100MB();//useless call for acceleration of JVM
        compared.append(getAverageTime100MB());
        compared.append(getAverageTime50MB());
        compared.append(getAverageTime25MB());
        compared.append(getAverageTime8MB());
        compared.append(getAverageTime1MB());
        compared.append(getAverageTime500KB());
        compared.append(getAverageTime100KB());
        compared.append(getAverageTime10KB());
        compared.append(getAverageTime1KB());
        compared.append(getAverageTime500B());
        return compared.toString();
    }
}
