package com.rostyslavprotsiv.model.action;

import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.LinkedList;
import java.util.List;

public class FindInCodeAction {

    public List<String> findComments(String path) throws IOException {
        List<String> comments = new LinkedList<>();
        StringBuilder sentence = new StringBuilder();
        StreamTokenizer tokenizer =
                new StreamTokenizer(new FileReader(path));
        tokenizer.ordinaryChar('/');
        tokenizer.eolIsSignificant(true);
        while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
            if (tokenizer.ttype == '/' || tokenizer.ttype == '*') {
                while (tokenizer.nextToken() != StreamTokenizer.TT_EOL) {
                    if (tokenizer.ttype == StreamTokenizer.TT_WORD) {
                        sentence.append(tokenizer.sval)
                                .append(" ");
                    }
                }
                if (!sentence.toString().isEmpty()) {
                    comments.add(sentence.toString().trim());
                    sentence = new StringBuilder();
                }
            }
        }
        return comments;
    }
}
