package com.rostyslavprotsiv.model.action;

import java.io.File;
import java.util.Arrays;
import java.util.Date;

public class FileAction {
    private String infoAboutAll = "";
    private int averageSpace = 4;

    public String getAllSubfiels(String path) {
        File parent = new File(path);
        recursiveFinding(parent, 0);
        return infoAboutAll;
    }

    private void recursiveFinding(File parent, int spaces) {
        infoAboutAll += generateSpaces(spaces) + "\u001B[34m" + parent.getName()
                + "\u001B[0m" + " size : " + parent.length() + ", last modified : "
                + new Date(parent.lastModified()) + "\n";
        if (parent.isDirectory()) {
            File[] children = parent.listFiles();
            Arrays.stream(children)
                    .forEach(f -> recursiveFinding(f, spaces + averageSpace));
        }
    }

    private String generateSpaces(int spaces) {
        StringBuilder resultSpaces = new StringBuilder();
        for (int i = 0; i < spaces; i++) {
            resultSpaces.append(" ");
        }
        return resultSpaces.toString();
    }
}
