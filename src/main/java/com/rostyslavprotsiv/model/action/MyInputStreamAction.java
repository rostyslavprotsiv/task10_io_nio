package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.stream.MyPushbackInputStream;

import java.io.FileInputStream;
import java.io.IOException;

public class MyInputStreamAction {
    private final String path = ResourceManager.INSTANCE.getString("path1");

    public String showExample() {
        StringBuilder result = new StringBuilder();
        int sizeOfBuffer = 4;
        try (MyPushbackInputStream inp =
                     new MyPushbackInputStream(new FileInputStream(path),
                             sizeOfBuffer)) {
            int firstByte;
            int secondByte;
            int thirdByte;
            int fourthByte;
            result.append((char) inp.read());
            result.append((char) inp.read());
            result.append((char) inp.read());
            result.append((char) inp.read());
            firstByte = inp.read();
            secondByte = inp.read();
            thirdByte = inp.read();
            fourthByte = inp.read();
            result.append((char) firstByte);
            result.append((char) secondByte);
            result.append((char) thirdByte);
            result.append((char) fourthByte);
            inp.unread(firstByte);
            inp.unread(secondByte);
            inp.unread(thirdByte);
            inp.unread(fourthByte);
            result.append((char) inp.read());
            result.append((char) inp.read());
            result.append((char) inp.read());
            result.append((char) inp.read());
            result.append((char) inp.read());
            result.append((char) inp.read());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
