package com.rostyslavprotsiv.model.action.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class NetServerThread extends Thread {
    private final Logger LOGGER = LogManager.getLogger(NetServerThread.class);
    private final int PORT = 8071;

    public void run() {
        try {
            ServerSocket server = new ServerSocket(PORT);
            LOGGER.info("initialized");
            while (true) {
                Socket socket = server.accept();
                LOGGER.info(socket.getInetAddress()
                        .getHostName() + " connected");
                ServerThread thread = new ServerThread(socket);
                thread.start();
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }
}
