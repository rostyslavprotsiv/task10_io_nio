package com.rostyslavprotsiv.model.action.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;

class ServerThread extends Thread {
    private final Logger LOGGER = LogManager.getLogger(ServerThread.class);
    private PrintStream os;
    private BufferedReader is;
    private InetAddress addr;

    public ServerThread(Socket s) throws IOException {
        os = new PrintStream(s.getOutputStream());
        is = new BufferedReader(new InputStreamReader(s.getInputStream()));
        addr = s.getInetAddress();
    }

    public void run() {
        String str = "";
        try {
            while (str != null) {
                LOGGER.info(addr.getHostName() + " : " + (str = is.readLine()));
            }
        } catch (IOException e) {
            String logMessage = "Disconnect";
            LOGGER.trace(logMessage);
            LOGGER.debug(logMessage);
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
            LOGGER.fatal(logMessage);
        } finally {
            disconnect();
        }
    }

    public void disconnect() {
        try {
            if (os != null) {
                os.close();
            }
            if (is != null) {
                is.close();
            }
            LOGGER.error(addr.getHostName() + " disconnecting");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            this.interrupt();
        }
    }
}
