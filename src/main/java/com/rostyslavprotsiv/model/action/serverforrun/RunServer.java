package com.rostyslavprotsiv.model.action.serverforrun;

import com.rostyslavprotsiv.model.action.server.NetServerThread;

public class RunServer {
    public static void main(String[] args) {
        new NetServerThread().run();
    }
}
