package com.rostyslavprotsiv.model.action.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class NetClient extends Thread {
    private final Logger LOGGER = LogManager.getLogger(NetClient.class);
    private final int PORT = 8071;

    @Override
    public void run() {
        Scanner scan = new Scanner(System.in, "UTF-8");
        try (Socket socket = new Socket(InetAddress.getLocalHost(), PORT);
             PrintStream ps = new PrintStream(socket.getOutputStream())) {
            while (true) {
                ps.println(scan.nextLine());
            }
        } catch (UnknownHostException e) {
            LOGGER.error("address inaccessible" + e);
        } catch (IOException e) {
            LOGGER.error("error of I/О thread" + e);
        }
    }
}
