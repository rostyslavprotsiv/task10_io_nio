package com.rostyslavprotsiv.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class Bottle implements Serializable {
    private Water water;
    private transient int weight;
    private int id;
    private double capacity;

    public Bottle(Water water, int weight, int id, double capacity) {
        this.water = water;
        this.weight = weight;
        this.id = id;
        this.capacity = capacity;
    }

    public Bottle(Water water, int id, double capacity) {
        this.water = water;
        this.id = id;
        this.capacity = capacity;
    }

    public Bottle() {
    }

    public void setWater(Water water) {
        this.water = water;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bottle bottle = (Bottle) o;
        return weight == bottle.weight &&
                id == bottle.id &&
                Double.compare(bottle.capacity, capacity) == 0 &&
                Objects.equals(water, bottle.water);
    }

    @Override
    public int hashCode() {
        return Objects.hash(water, weight, id, capacity);
    }

    @Override
    public String toString() {
        return "Bottle{" +
                "water=" + water +
                ", weight=" + weight +
                ", id=" + id +
                ", capacity=" + capacity +
                '}';
    }
}
