package com.rostyslavprotsiv.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class Water implements Serializable {
    private String name;
    private transient int price;

    public Water(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Water() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Water water = (Water) o;
        return price == water.price &&
                Objects.equals(name, water.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }

    @Override
    public String toString() {
        return "Water{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
