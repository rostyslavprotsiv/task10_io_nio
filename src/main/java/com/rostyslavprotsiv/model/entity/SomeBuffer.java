//package com.rostyslavprotsiv.model.entity;
//
//import java.nio.*;
//
//public class SomeBuffer extends ByteBuffer {
//
//    @Override
//    public ByteBuffer get(byte[] bytes, int i, int i1) {
//        return super.get(bytes, i, i1);
//    }
//
//    @Override
//    public ByteBuffer get(byte[] bytes) {
//        return super.get(bytes);
//    }
//
//    @Override
//    public ByteBuffer put(ByteBuffer byteBuffer) {
//        return super.put(byteBuffer);
//    }
//
//    @Override
//    public ByteBuffer put(byte[] bytes, int i, int i1) {
//        return super.put(bytes, i, i1);
//    }
//
//    @Override
//    public String toString() {
//        return super.toString();
//    }
//
//    @Override
//    public int hashCode() {
//        return super.hashCode();
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        return super.equals(o);
//    }
//
//    @Override
//    public int compareTo(ByteBuffer byteBuffer) {
//        return super.compareTo(byteBuffer);
//    }
//
//    @Override
//    public ByteBuffer slice() {
//        return null;
//    }
//
//    @Override
//    public ByteBuffer duplicate() {
//        return null;
//    }
//
//    @Override
//    public ByteBuffer asReadOnlyBuffer() {
//        return null;
//    }
//
//    @Override
//    public byte get() {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer put(byte b) {
//        return null;
//    }
//
//    @Override
//    public byte get(int i) {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer put(int i, byte b) {
//        return null;
//    }
//
//    @Override
//    public ByteBuffer compact() {
//        return null;
//    }
//
//    @Override
//    public boolean isDirect() {
//        return false;
//    }
//
//    @Override
//    public char getChar() {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putChar(char c) {
//        return null;
//    }
//
//    @Override
//    public char getChar(int i) {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putChar(int i, char c) {
//        return null;
//    }
//
//    @Override
//    public CharBuffer asCharBuffer() {
//        return null;
//    }
//
//    @Override
//    public short getShort() {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putShort(short i) {
//        return null;
//    }
//
//    @Override
//    public short getShort(int i) {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putShort(int i, short i1) {
//        return null;
//    }
//
//    @Override
//    public ShortBuffer asShortBuffer() {
//        return null;
//    }
//
//    @Override
//    public int getInt() {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putInt(int i) {
//        return null;
//    }
//
//    @Override
//    public int getInt(int i) {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putInt(int i, int i1) {
//        return null;
//    }
//
//    @Override
//    public IntBuffer asIntBuffer() {
//        return null;
//    }
//
//    @Override
//    public long getLong() {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putLong(long l) {
//        return null;
//    }
//
//    @Override
//    public long getLong(int i) {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putLong(int i, long l) {
//        return null;
//    }
//
//    @Override
//    public LongBuffer asLongBuffer() {
//        return null;
//    }
//
//    @Override
//    public float getFloat() {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putFloat(float v) {
//        return null;
//    }
//
//    @Override
//    public float getFloat(int i) {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putFloat(int i, float v) {
//        return null;
//    }
//
//    @Override
//    public FloatBuffer asFloatBuffer() {
//        return null;
//    }
//
//    @Override
//    public double getDouble() {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putDouble(double v) {
//        return null;
//    }
//
//    @Override
//    public double getDouble(int i) {
//        return 0;
//    }
//
//    @Override
//    public ByteBuffer putDouble(int i, double v) {
//        return null;
//    }
//
//    @Override
//    public DoubleBuffer asDoubleBuffer() {
//        return null;
//    }
//
//    @Override
//    public boolean isReadOnly() {
//        return false;
//    }
//
//    @Override
//    byte _get(int i) {                          // package-private
//        return 0;
//    }
//
//    @Override
//    void _put(int i, byte b) {                  // package-private
//    }
//
//
//}
