package com.rostyslavprotsiv.model.creator;

import com.rostyslavprotsiv.model.entity.Bottle;
import com.rostyslavprotsiv.model.entity.Water;

public class BottleCreator {
//imitation of reading from DB
    public Bottle readFromDB() {
        //process of reading from DB
        return new Bottle(new Water("Wert", 4), 3, 3, 5.6);
    }
}
